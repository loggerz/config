# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";
  boot.loader.grub.useOSProber = true;
  boot.initrd.kernelModules = [ "amdgpu" ];

  networking.hostName = "karadev"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Zurich";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.videoDrivers = [ "amdgpu" ]; 
 
  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.wayland.enable = true;
  services.xserver.desktopManager.plasma6.enable = true;
  services.xserver.displayManager.defaultSession = "plasma";

  # Enable OpenGL
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "fr";
    variant = "mac";
  };

  # Configure console keymap
  console.keyMap = "fr";

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.loggerz = {
    isNormalUser = true;
    description = "loggerz";
    extraGroups = [ "networkmanager" "wheel" ];
    shell = pkgs.zsh;
    packages = with pkgs; [
      firefox
      kate
      rustdesk
      android-studio
      sdkmanager
      _1password-gui
      chromium
      jetbrains.webstorm
      jetbrains.rust-rover
      jetbrains.datagrip
    ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkod1suP3di/YUasrARvw5cUuL0R9O2stgrwNfC/OFpbMUImDseW0W7HXktGIu2FgBUUo+BIK5PUj6QSS7jxjj0eFfpubySjDkJkwwYqO9SkKC7g05cvkSyOjHxBQ+8cLHlg1nZz02MIKnJFUnNSc5iOY2uT3wwfa0z88N9N/w5Ey/K8+VY8ml68juYhKRNyJzDvXwUJ4vgzUlmFriK90pKJXyzqniWYXrt/+g9V3YHODpBKLnGbKj00FndXE8mIIj7IRUwGwB2uMRQhxiOqT+HQXziHCSx218HZhvPoYHSKQBJTvpx6WjKozKbUyzioGTrYtfZvYRCLtxcwMI6S30uWjeKVMYL+xgLxPI6G9AuUQkK4HXF95MPKbn+k5IcD/jQmn2ZrzO8HByPXg/KeAZns+lGhmwCTG6zlFZctRLCFzLIlby/FRmnQX7XErjv1of/8+vJqKlrPbeXP4LyLqlWVwKJJ0Mb6z7S0T5UnqG6qGuDrh3Ey8l1l+KqWjF0Fk= loggerz@mbpsy"
    ];
  };

  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "loggerz";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    gnugrep
    zsh
    htop
    btop
    vim
    wget
    git 
    curl
    gnupg
    openssl
    sshfs
    dig
    jq
    neovim
    vscodium
    brave
    libsForQt5.filelight
    # Neovim
    tree-sitter
    luajitPackages.jsregexp
    lazygit
    fd
    xsel
    xclip
    ripgrep
    fzf
    # Dev
    cargo
    gcc
    gdb
    gnumake
    sbt
    coursier # scala dep
    jdk17 # scala dep
    unzip
    nodejs_21
    python3
    android-tools
    flutter
    # remote
    xpra
    run-scaled
    ffmpeg_5
    glxinfo
    clinfo
    vulkan-tools
    pciutils
    usbutils
  ];

  
  environment.variables = {
    ROC_ENABLE_PRE_VEGA = "1"; # https://nixos.wiki/wiki/AMD_GPU#Radeon_500_series_.28aka_Polaris.29
  };

  programs.zsh = {
    enable = true;
    shellAliases = {
      config = "sudo vim /etc/nixos/configuration.nix";
      update = "sudo nixos-rebuild switch";
    };
    ohMyZsh = {
      enable = true;
      plugins = ["git" "themes" "kubectl"];
      theme = "agnoster";
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      X11Forwarding = true;
    };
  };

  services.x2goserver.enable = true;

  services.qemuGuest.enable = true;

  virtualisation.docker = {
    enable = true;
    rootless = {
      enable = true;
      setSocketVariable = true;  
    };
  };

  services.zerotierone = {
    enable = true;
    joinNetworks = [ "8286ac0e473b60ff" ];
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 21118 4000 5000 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}

