# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = false;
  boot.loader.systemd-boot.graceful = true;

  boot.initrd.luks.devices."luks-ab12b314-185f-4b21-9bbc-019a5cab8f74".device = "/dev/disk/by-uuid/ab12b314-185f-4b21-9bbc-019a5cab8f74";

  networking.hostName = "linsy"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Zurich";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.wayland.enable = true;
  services.xserver.desktopManager.plasma6.enable = true;
  services.xserver.displayManager.defaultSession = "plasma";

  # Enable OpenGL
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  # Load nvidia driver for Xorg and Wayland
  # services.xserver.videoDrivers = ["nvidia"]; # or "nvidiaLegacy470 etc.

  # hardware.nvidia = {

  #   # Modesetting is required.
  #   modesetting.enable = true;

  #   # Nvidia power management. Experimental, and can cause sleep/suspend to fail.
  #   # Enable this if you have graphical corruption issues or application crashes after waking
  #   # up from sleep. This fixes it by saving the entire VRAM memory to /tmp/ instead 
  #   # of just the bare essentials.
  #   powerManagement.enable = false;

  #   # Fine-grained power management. Turns off GPU when not in use.
  #   # Experimental and only works on modern Nvidia GPUs (Turing or newer).
  #   powerManagement.finegrained = false;

  #   # Use the NVidia open source kernel module (not to be confused with the
  #   # independent third-party "nouveau" open source driver).
  #   # Support is limited to the Turing and later architectures. Full list of 
  #   # supported GPUs is at: 
  #   # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus 
  #   # Only available from driver 515.43.04+
  #   # Currently alpha-quality/buggy, so false is currently the recommended setting.
  #   open = false;

  #   # Enable the Nvidia settings menu,
	#   # accessible via `nvidia-settings`.
  #   nvidiaSettings = true;

  #   # https://nixos.wiki/wiki/Nvidia#Screen_Tearing_Issues
  #   # forceFullCompositionPipeline = false;

  #   # Optionally, you may need to select the appropriate driver version for your specific GPU.
  #   # https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/os-specific/linux/nvidia-x11/default.nix
  #   package = config.boot.kernelPackages.nvidiaPackages.stable;
  # };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "fr";
    variant = "mac";
  };

  # Configure console keymap
  console.keyMap = "fr";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable Bluetooth controller
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.loggerz = {
    isNormalUser = true;
    description = "loggerz";
    extraGroups = [ "networkmanager" "wheel" "wireshark"];
    shell = pkgs.zsh;
    packages = with pkgs; [
      firefox
      kate
      thunderbird-bin
      plexamp
      telegram-desktop
      discord
      signal-desktop
      tigervnc
      rustdesk # heavy to compile
      x2goclient
      gimp
      # obsidian # out-of-date
      transmission_4
      yt-dlp
      spotdl
      qalculate-gtk
      jetbrains.webstorm
      jetbrains.rust-rover
      jetbrains.datagrip
      jetbrains.clion
      rare # GUI for legendary FOSS launcher for Epic Games
      protonup-qt
      lutris
      legendary-gl
      wineWowPackages.waylandFull
      peek
      vlc
      obs-studio
      wireshark
      k6
      ollama
    ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkod1suP3di/YUasrARvw5cUuL0R9O2stgrwNfC/OFpbMUImDseW0W7HXktGIu2FgBUUo+BIK5PUj6QSS7jxjj0eFfpubySjDkJkwwYqO9SkKC7g05cvkSyOjHxBQ+8cLHlg1nZz02MIKnJFUnNSc5iOY2uT3wwfa0z88N9N/w5Ey/K8+VY8ml68juYhKRNyJzDvXwUJ4vgzUlmFriK90pKJXyzqniWYXrt/+g9V3YHODpBKLnGbKj00FndXE8mIIj7IRUwGwB2uMRQhxiOqT+HQXziHCSx218HZhvPoYHSKQBJTvpx6WjKozKbUyzioGTrYtfZvYRCLtxcwMI6S30uWjeKVMYL+xgLxPI6G9AuUQkK4HXF95MPKbn+k5IcD/jQmn2ZrzO8HByPXg/KeAZns+lGhmwCTG6zlFZctRLCFzLIlby/FRmnQX7XErjv1of/8+vJqKlrPbeXP4LyLqlWVwKJJ0Mb6z7S0T5UnqG6qGuDrh3Ey8l1l+KqWjF0Fk= loggerz@mbpsy"
    ];
  };

  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "loggerz";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    gnugrep
    zsh
    kitty
    htop
    btop
    vim
    wget
    git
    curl
    gnupg
    openssl
    sshfs
    dig
    jq
    neovim
    vscodium
    kopia
    brave
    chromium
    libsForQt5.yakuake
    libsForQt5.filelight
    libsForQt5.kcmutils
    remmina
    _1password-gui
    openconnect
    # Neovim
    tree-sitter
    luajitPackages.jsregexp
    lazygit
    fd
    xsel
    xclip
    ripgrep
    fzf
    # Dev
    cargo
    rustup
    pkg-config
    gcc
    gdb
    gnumake
    gparted
    sbt
    jdk17
    unzip
    nodejs_21
    python3
    kubeseal
    # kopia
    # remote
    xpra
    ffmpeg_5
    glxinfo
    pciutils
    usbutils
    lm_sensors
    glances
    iperf
    nmap
    tcpdump
    arp-scan
    smartmontools
    kubectl
    kubernetes-helm
    screen
    flutter
  ];

  programs.zsh = {
    enable = true;
    shellAliases = {
      config = "vim /home/loggerz/dev/loggerz/config/nix/linsy/configuration.nix";
      update = "sudo nixos-rebuild switch -I nixos-config=/home/loggerz/dev/loggerz/config/nix/linsy/configuration.nix";
      v = "nvim";
      infra = "cd ~/dev/agepinfo/infra";
    };
    ohMyZsh = {
      enable = true;
      plugins = ["git" "themes" "kubectl"];
      theme = "agnoster";
    };
  };

  programs.steam.enable = true;

  programs.wireshark.enable = true;

  programs.virt-manager.enable = true;

  virtualisation.docker = {
    enable = true;
    rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
  };

  services.syncthing = {
    enable = true;
    user = "loggerz";
    dataDir = "/home/loggerz/Sync";
    configDir = "/home/loggerz/.config/syncthing";
    settings = {
      devices = {
        karaserv = {
          id = "NACTOB2-22OL6DK-ZUQ3LSZ-W2MGFUL-GXDIZ7N-K4SGTZY-LOHN6PP-FR33ZQ5";
        };
        mbpsy = {
          id = "DTFY7SU-YDFDIUD-7RXPVBG-RTHYCQ4-AKYHBMB-GARCE5W-KVVALVH-2CGDIQT";
        };
      };
      folders = {
        "Documents" = {
          path = "/home/loggerz/Sync/Documents";
          devices = [ "karaserv" "mbpsy" ];
        };
      };
    };
  };

   services.zerotierone = {
    enable = true;
    joinNetworks = [ "8286ac0e473b60ff" ];
  };

  services.ollama = {
    enable = true;
    acceleration = "cuda";
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 3000 4000 5000 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
