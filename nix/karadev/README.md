This was the first attempt at creating a dev VM on Karaserv

Issues:
- Legacy BIOS boot, not UEFI. Lot of work required for transition. Issues met lead to abort
- NixOS is not meant to be used for complete IDE work without a nix-shell
- Frequent packages are out-of-date, more work required to install them anyway with a local patch
