# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let unstable = import <nixos-unstable> { config = { allowUnfree = true; }; }; in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # https://nixos.wiki/wiki/Storage_optimization
  nix.optimise.automatic = true;
 
  networking.hostName = "mbpsy"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  networking.hosts = {
    "127.0.0.1" = [ "kubernetes" ];
  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Zurich";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "fr";
    xkbVariant = "mac";
  };

  # Configure console keymap
  console.keyMap = "fr";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable Bluetooth controller
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  hardware.logitech.wireless.enable = true;

  users.groups = {
    plugdev = {};
  };  

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.loggerz = {
    isNormalUser = true;
    description = "loggerz";
    extraGroups = [ "networkmanager" "wheel" "input" "plugdev"];
    shell = pkgs.zsh;
    packages = with pkgs; [
      firefox
      kate
      thunderbird-bin
      plexamp
      unstable.telegram-desktop
      discord
      signal-desktop
      tigervnc
      # rustdesk # heavy to compile
      x2goclient
      gimp
      # obsidian
      qalculate-gtk
      signaturepdf
      virt-viewer
      ## tools
      yt-dlp-light
      spotdl
      vlc
      peek
      bitwarden
      jetbrains.gateway
      solaar
      logitech-udev-rules
      vscode-fhs
      pdftk
      pdfgrep
      pdfchain
      pdfslicer
      handbrake
      barrier
      gtg
    ];
  };

  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "loggerz";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    gnugrep
    zsh
    kitty
    htop
    btop
    vim
    wget
    git 
    curl
    gnupg
    openssl_3_1
    sshfs
    dig
    traceroute
    jq
    neovim
    unstable.vscodium
    kopia
    brave
    chromium
    libsForQt5.yakuake  
    libsForQt5.filelight
    libsForQt5.akonadi
    libsForQt5.accounts-qt
    libsForQt5.kaccounts-integration
    libsForQt5.kaccounts-providers
    libsForQt5.signond
    libsForQt5.qoauth
    libsForQt5.korganizer
    libsForQt5.calendarsupport
    libsForQt5.merkuro
    remmina
    _1password-gui
    openconnect
    # Neovim
    tree-sitter
    luajitPackages.jsregexp
    lazygit
    fd
    xsel
    xclip
    ripgrep
    fzf
    # Dev
    cargo
    gcc
    gdb
    gnumake
    gnupg
    gparted
    sbt
    unzip
    nodejs_21
    python3
    postgresql
    kubectl
    unstable.kubernetes-helm
    unstable.fission
    kubeseal
    fusuma # trackpad utility
    # kopia
    ## remote 
    xpra
    ffmpeg_5
    x264
    usbredir
    spice-gtk
    usbutils
    libsForQt5.kcolorpicker
    redis
    # dive # heavy?
  ];

  # https://discourse.nixos.org/t/having-an-issue-with-virt-manager-not-allowing-usb-passthrough/6272/3
  virtualisation.spiceUSBRedirection.enable = true;

  programs.gnupg.agent.enable = true;

  environment.variables = {
    KUBECONFIG = "/home/loggerz/Sync/Documents/EPFL/AGEPINFO/INFRA/K22/kubeconfig.yml";
    EDITOR = "vim";
  };

  programs.zsh = {
    enable = true;
    shellAliases = {
      config = "vim /home/loggerz/dev/loggerz/config/nix/mbpsy/configuration.nix";
      update = "sudo nixos-rebuild switch -I nixos-config=/home/loggerz/dev/loggerz/config/nix/mbpsy/configuration.nix";
      v = "nvim";
      infra = "cd ~/dev/agepinfo/infra && codium .";
      k22-proxy = "ssh -L 6443:localhost:6443 flep5";
      agepinfo = "cd ~/Sync/Documents/EPFL/AGEPINFO";
    };
    ohMyZsh = {
      enable = true;
      plugins = ["git" "themes" "kubectl"];
      theme = "agnoster";
    };
  };

  programs.virt-manager.enable = true;

  virtualisation.docker = {
    enable = true;
    rootless = {
      enable = true;
      setSocketVariable = true;  
    };
  };

  services.syncthing = {
    enable = true;
    user = "loggerz";
    dataDir = "/home/loggerz/Sync";
    configDir = "/home/loggerz/.config/syncthing";
    settings = {
      devices = {
        karaserv = {
          id = "NACTOB2-22OL6DK-ZUQ3LSZ-W2MGFUL-GXDIZ7N-K4SGTZY-LOHN6PP-FR33ZQ5";
        };
        linsy = {
          id = "I2ODJ65-XGQHKIT-MHLICTV-5PWZCCX-OMLZVAJ-AVQZ2WE-GVLQXW4-XRARBAQ";
        };
      };
      folders = {
        "Documents" = {
          path = "/home/loggerz/Sync/Documents";
          devices = [ "karaserv" "linsy" ];
        };
      };
    };
  };

  services.zerotierone = {
    enable = true;
    joinNetworks = [ "8286ac0e473b60ff" ];
  };

  services.avahi = {
    enable = true;
    nssmdns = true;
    openFirewall = true;
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22000 3000 4000 5000 24800 ];
  networking.firewall.allowedUDPPorts = [ 22000 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
